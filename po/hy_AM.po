# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# E-Hayq LLC <www.ehayq.am>, 2018
# Real School <localization@ehayq.am>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Xfce4-session\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-01 00:31+0200\n"
"PO-Revision-Date: 2020-03-31 22:31+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Armenian (Armenia) (http://www.transifex.com/xfce/xfce4-session/language/hy_AM/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hy_AM\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../xfce.desktop.in.h:1
msgid "Xfce Session"
msgstr "Xfce նիստ"

#: ../xfce.desktop.in.h:2
msgid "Use this session to run Xfce as your desktop environment"
msgstr "Գործածէք այս նիստը, գործարկելու համար Xfce աշխատասեղանի միջավայրը"

#: ../libxfsm/xfsm-util.c:331
msgid "Session"
msgstr "Նիստ"

#: ../libxfsm/xfsm-util.c:342
msgid "Last accessed"
msgstr "Վերջին գործածում"

#: ../scripts/xscreensaver.desktop.in.h:1
msgid "Screensaver"
msgstr "Էկրանապահ"

#: ../scripts/xscreensaver.desktop.in.h:2
msgid "Launch screensaver and locker program"
msgstr "Միացնել մեկնարկի վահանակը եւ կողպման ծրագիրը"

#: ../settings/main.c:99
msgid "Settings manager socket"
msgstr "Կարգաւորումների կառավարչի բնիկ"

#: ../settings/main.c:99
msgid "SOCKET ID"
msgstr "ԲՆԻԿԻ ID"

#: ../settings/main.c:100
msgid "Version information"
msgstr "Տարբերակի վերաբերեալ տեղեկոյթ"

#: ../settings/main.c:111 ../xfce4-session/main.c:333
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Մուտքագրէք «%s --help» աջակցութեան համար։"

#: ../settings/main.c:123 ../xfce4-session/main.c:343
#: ../xfce4-session-logout/main.c:146
msgid "The Xfce development team. All rights reserved."
msgstr "Xfce-ի մշակման խումբ։ Բոլոր իրաւունքները պաշտպանուած են։"

#: ../settings/main.c:124 ../xfce4-session/main.c:344
#: ../xfce4-session-logout/main.c:149
#, c-format
msgid "Please report bugs to <%s>."
msgstr "Սխալի դէպքում կարող էք դիմել <%s> հասցէով։"

#: ../settings/main.c:133 ../xfce4-session/main.c:352
msgid "Unable to contact settings server"
msgstr "Չյաջողուեց միանալ կարգաւորումների սպասարկչին"

#: ../settings/main.c:152
msgid "Unable to create user interface from embedded definition data"
msgstr "Չյաջողուեց ստեղծել աւգտատիրոջ միջերես, ներկառուցուած տուեալներից։"

#: ../settings/main.c:165
msgid "App_lication Autostart"
msgstr "Ինքնագործարկուող _ծրագրեր"

#: ../settings/main.c:171
msgid "Currently active session:"
msgstr "Ներկայումս գործուն նիստ․"

#: ../settings/session-editor.c:63
msgid "If running"
msgstr "Եթե գործուն է"

#: ../settings/session-editor.c:64
msgid "Always"
msgstr "Միշտ"

#: ../settings/session-editor.c:65
msgid "Immediately"
msgstr "Անմիջապէս"

#: ../settings/session-editor.c:66
msgid "Never"
msgstr "Երբեք"

#: ../settings/session-editor.c:138
msgid "Session Save Error"
msgstr "Նիստի պահման սխալ"

#: ../settings/session-editor.c:139
msgid "Unable to save the session"
msgstr "Չյաջողուեց պահել նիստը"

#: ../settings/session-editor.c:141 ../settings/session-editor.c:313
#: ../xfce4-session/xfsm-manager.c:1281
#: ../settings/xfce4-session-settings.ui.h:3
msgid "_Close"
msgstr "_Փակել"

#: ../settings/session-editor.c:198
msgid "Clear sessions"
msgstr "Մաքրել նիստերը"

#: ../settings/session-editor.c:199
msgid "Are you sure you want to empty the session cache?"
msgstr "Մաքրե՞լ նիստի շտեմը։"

#: ../settings/session-editor.c:200
msgid ""
"The saved states of your applications will not be restored during your next "
"login."
msgstr "Ծրագրերի պահուած վիճակը չի վերականգուի յաջորդ միացմանը։"

#: ../settings/session-editor.c:201 ../settings/session-editor.c:288
#: ../settings/xfae-dialog.c:77 ../xfce4-session/xfsm-manager.c:682
#: ../xfce4-session/xfsm-logout-dialog.c:221
msgid "_Cancel"
msgstr "_Չեղարկել"

#: ../settings/session-editor.c:202
msgid "_Proceed"
msgstr "_Շարունակել"

#: ../settings/session-editor.c:240
#, c-format
msgid "You might need to delete some files manually in \"%s\"."
msgstr "Անհրաժեշտ կը լինի ձեռքով ջնջել մի քանի նիշ \"%s\"֊ում։"

#: ../settings/session-editor.c:243
msgid "All Xfce cache files could not be cleared"
msgstr "Չյաջողուեց մաքրել Xfce-ի ամբողջ շտեմը"

#: ../settings/session-editor.c:282
#, c-format
msgid "Are you sure you want to terminate \"%s\"?"
msgstr "Կանգնեցնե՞լ «%s»֊ը։"

#: ../settings/session-editor.c:285 ../settings/session-editor.c:310
msgid "Terminate Program"
msgstr "Կանգնեցնե՞լ ծրագիրը"

#: ../settings/session-editor.c:287
msgid ""
"The application will lose any unsaved state and will not be restarted in "
"your next session."
msgstr "Ծրագիրը կը կորցնի ցանկացած չպահուած վիճակ եւ չի վերագործարկուի յաջորդ նիստի ժամանակ։"

#: ../settings/session-editor.c:289 ../settings/xfce4-session-settings.ui.h:19
msgid "_Quit Program"
msgstr "_Դուրս գալ ծրագրից"

#: ../settings/session-editor.c:311
msgid "Unable to terminate program."
msgstr "Չյաջողուեց կանգնեցնել ծրագիրը"

#: ../settings/session-editor.c:536
msgid "(Unknown program)"
msgstr "(Անյայտ ծրագիր)"

#: ../settings/session-editor.c:768
msgid "Priority"
msgstr "Առաջնահերթութիւն"

#: ../settings/session-editor.c:776
msgid "PID"
msgstr "ԳՆՀ"

#: ../settings/session-editor.c:782 ../settings/xfae-window.c:172
msgid "Program"
msgstr "Ծրագիր"

#: ../settings/session-editor.c:807
msgid "Restart Style"
msgstr "Վերագործարկման ձեւ"

#: ../settings/xfae-dialog.c:78 ../xfce4-session/xfsm-manager.c:684
msgid "_OK"
msgstr "_Բարի"

#: ../settings/xfae-dialog.c:82 ../settings/xfae-window.c:220
msgid "Add application"
msgstr "Աւելացնել ծրագիր"

#: ../settings/xfae-dialog.c:96
msgid "Name:"
msgstr "Անուն․"

#: ../settings/xfae-dialog.c:111
msgid "Description:"
msgstr "Նկարագրութիւն․"

#: ../settings/xfae-dialog.c:125 ../settings/xfae-model.c:548
msgid "Command:"
msgstr "Հրաման․"

#: ../settings/xfae-dialog.c:138
msgid "Trigger:"
msgstr "Ձգան․"

#: ../settings/xfae-dialog.c:209
msgid "Select a command"
msgstr "Ընտրել հրաման"

#: ../settings/xfae-dialog.c:212
msgid "Cancel"
msgstr "Փակել"

#: ../settings/xfae-dialog.c:213
msgid "OK"
msgstr "OK"

#: ../settings/xfae-dialog.c:260 ../settings/xfae-window.c:242
msgid "Edit application"
msgstr "Խմբագրել ծրագիրը"

#: ../settings/xfae-model.c:87
msgid "on login"
msgstr "մուտքի ժամանակ"

#: ../settings/xfae-model.c:88
msgid "on logout"
msgstr "դուրս գալուց"

#: ../settings/xfae-model.c:89
msgid "on shutdown"
msgstr "անջատման ժամանակ"

#: ../settings/xfae-model.c:90
msgid "on restart"
msgstr "վերագործարկման ժամանակ"

#: ../settings/xfae-model.c:91
msgid "on suspend"
msgstr "նիրհման ժամանակ"

#: ../settings/xfae-model.c:92
msgid "on hibernate"
msgstr "քնի ժամանակ"

#: ../settings/xfae-model.c:93
msgid "on hybrid sleep"
msgstr "համակցուած քնի ժամանակ"

#: ../settings/xfae-model.c:94
msgid "on switch user"
msgstr "աւգտատիրոջ փոփոխման ժամանակ"

#: ../settings/xfae-model.c:304 ../settings/xfae-model.c:972
#: ../settings/xfae-model.c:1030
#, c-format
msgid "Failed to open %s for writing"
msgstr "Չյաջողուեց բացել %s նիշը գրառման համար"

#: ../settings/xfae-model.c:681
#, c-format
msgid "Failed to unlink %s: %s"
msgstr "Չյաջողուեց ապայղել %s․ %s֊ը"

#: ../settings/xfae-model.c:772
#, c-format
msgid "Failed to create file %s"
msgstr "Չյաջողուեց ստեղծել %s նիշը"

#: ../settings/xfae-model.c:796
#, c-format
msgid "Failed to write file %s"
msgstr "Չյաջողուեց գրառել %s նիշը"

#: ../settings/xfae-model.c:856
#, c-format
msgid "Failed to open %s for reading"
msgstr "Չյաջողուեց բացել %s նիշը ընթերցման համար"

#: ../settings/xfae-window.c:98
msgid "Failed to set run hook"
msgstr "Չյաջողուեց սահմանել բեռնման կեռիկը"

#: ../settings/xfae-window.c:193
msgid "Trigger"
msgstr "Շնիկ"

#: ../settings/xfae-window.c:229
msgid "Remove application"
msgstr ""

#: ../settings/xfae-window.c:306
msgid "Add"
msgstr "Աւելացնել"

#: ../settings/xfae-window.c:312
msgid "Remove"
msgstr "Հեռացնել"

#: ../settings/xfae-window.c:366
#, c-format
msgid "Failed adding \"%s\""
msgstr "Չյաջողուեց յաւելել «%s»֊ը"

#: ../settings/xfae-window.c:397 ../settings/xfae-window.c:411
msgid "Failed to remove item"
msgstr "Չյաջողուեց հեռացնել տարրը"

#: ../settings/xfae-window.c:440
msgid "Failed to edit item"
msgstr "Չյաջողուեց խմբագրել տարրը"

#: ../settings/xfae-window.c:460
#, c-format
msgid "Failed to edit item \"%s\""
msgstr "Չյաջողուեց խմբագրել «%s» տարրը"

#: ../settings/xfae-window.c:488
msgid "Failed to toggle item"
msgstr "Չյաջողուեց անջատել տարրը"

#: ../xfce4-session/main.c:77
msgid "Disable binding to TCP ports"
msgstr "Անջատել կապակցումը TCP բնիկների հետ"

#: ../xfce4-session/main.c:78 ../xfce4-session-logout/main.c:102
msgid "Print version information and exit"
msgstr "Տպել տարբերակի վերաբերեալ տեղեկոյթը եւ դուրս գալ"

#: ../xfce4-session/xfsm-chooser.c:147
msgid "Session Manager"
msgstr "Նիստի կառավարիչ"

#: ../xfce4-session/xfsm-chooser.c:168
msgid ""
"Choose the session you want to restore. You can simply double-click the "
"session name to restore it."
msgstr "Ընտրէք նիստը, որը ցանկանում էք վերականգնել։ Կարող էք երկկատացնել նիստի անուան վրայ, այն վերականգնելու համար։"

#: ../xfce4-session/xfsm-chooser.c:184
msgid "Create a new session."
msgstr "Ստեղծել նոր նիստ։"

#: ../xfce4-session/xfsm-chooser.c:191
msgid "Delete a saved session."
msgstr "Ջնջել պահուած նիստ։"

#. "Logout" button
#: ../xfce4-session/xfsm-chooser.c:202
#: ../xfce4-session-logout/xfce4-session-logout.desktop.in.h:1
msgid "Log Out"
msgstr "Աւարտել նիստը"

#: ../xfce4-session/xfsm-chooser.c:204
msgid "Cancel the login attempt and return to the login screen."
msgstr "Չեղարկել դուրս գալու փորձն ու վերադառնալ մուտքի դաշտ։"

#. "Start" button
#: ../xfce4-session/xfsm-chooser.c:211
msgid "Start"
msgstr "Գործարկել"

#: ../xfce4-session/xfsm-chooser.c:212
msgid "Start an existing session."
msgstr "Միացնել գոյութիւն ունեցող նիստը։"

#: ../xfce4-session/xfsm-dns.c:78
msgid "(Unknown)"
msgstr "(Անյայտ)"

#: ../xfce4-session/xfsm-dns.c:152
#, c-format
msgid ""
"Could not look up internet address for %s.\n"
"This will prevent Xfce from operating correctly.\n"
"It may be possible to correct the problem by adding\n"
"%s to the file /etc/hosts on your system."
msgstr "Անհնար է գտնել %s-ի համացանցային հասցէն:\nՍա կը խանգարի Xfce-ի ճշգրիտ գործարկմանը։\nՀնարաւոր է շտկել խնդիրը,\nձեր համակարգում /etc/hosts նիշին %s աւելացնելու դէպքում։"

#: ../xfce4-session/xfsm-dns.c:159
msgid "Continue anyway"
msgstr "Շարունակել"

#: ../xfce4-session/xfsm-dns.c:160
msgid "Try again"
msgstr "Կրկին փորձել"

#: ../xfce4-session/xfsm-manager.c:566
#, c-format
msgid ""
"Unable to determine failsafe session name.  Possible causes: xfconfd isn't "
"running (D-Bus setup problem); environment variable $XDG_CONFIG_DIRS is set "
"incorrectly (must include \"%s\"), or xfce4-session is installed "
"incorrectly."
msgstr "Չյաջողուեց որոշել անվտանգ նիստի անունը։ Հնարաւոր պատճառներից են՝ xfconfd֊ին չի գործարկուում (D-Bus֊ի տեղադրման սխալ), միջավայրի $XDG_CONFIG_DIRS փոփոխականը ճիշը չէ տեղակայուած («%s»֊ը պարտադիր է) կամ էլ՝ xfce4-session֊ը սխալ է տեղակայուած։"

#: ../xfce4-session/xfsm-manager.c:577
#, c-format
msgid ""
"The specified failsafe session (\"%s\") is not marked as a failsafe session."
msgstr "Անվտանգ սահմանուած («%s») նիստը, չի նշուում որպէս անվտանգ։"

#: ../xfce4-session/xfsm-manager.c:610
msgid "The list of applications in the failsafe session is empty."
msgstr "Ծրագրերի ցուցակը անվտանգ նիստում՝ դատարկ է։"

#: ../xfce4-session/xfsm-manager.c:696
msgid "Name for the new session"
msgstr "Ընտրել նոր նիստի անունը․"

#. FIXME: migrate this into the splash screen somehow so the
#. * window doesn't look ugly (right now no WM is running, so it
#. * won't have window decorations).
#: ../xfce4-session/xfsm-manager.c:774
msgid "Session Manager Error"
msgstr "Նիստի կառավարչի սխալ"

#: ../xfce4-session/xfsm-manager.c:776
msgid "Unable to load a failsafe session"
msgstr "Չյաջողուեց բեռնել անվտանգ նիստ"

#: ../xfce4-session/xfsm-manager.c:778
msgid "_Quit"
msgstr "_Դուրս գալ"

#: ../xfce4-session/xfsm-manager.c:1271
msgid "Shutdown Failed"
msgstr "Անջատման ձախողում"

#: ../xfce4-session/xfsm-manager.c:1274
msgid "Failed to suspend session"
msgstr "Նիստի նիրհման ձախողում"

#: ../xfce4-session/xfsm-manager.c:1276
msgid "Failed to hibernate session"
msgstr "Նիստի քնի ձախողում"

#: ../xfce4-session/xfsm-manager.c:1278
msgid "Failed to hybrid sleep session"
msgstr "Նիստի համակցուած քնի ձախողում"

#: ../xfce4-session/xfsm-manager.c:1279
msgid "Failed to switch user"
msgstr "Աւգտատիրոջ փոփոխման ձախողում"

#: ../xfce4-session/xfsm-manager.c:1585
#, c-format
msgid "Can only terminate clients when in the idle state"
msgstr "Հնարաւոր է կասեցնել սպասառուները, միայն անգործուն վիճակում"

#: ../xfce4-session/xfsm-manager.c:2247
msgid "Session manager must be in idle state when requesting a checkpoint"
msgstr "Նիստի կառավարիչը պէտք է չգործի, ստուգակէտի հարցման ժամանակ"

#: ../xfce4-session/xfsm-manager.c:2317 ../xfce4-session/xfsm-manager.c:2337
msgid "Session manager must be in idle state when requesting a shutdown"
msgstr "Նիստի կառավարիչը պէտք չգործի, անջատման հրամանի հարցման ժամանակ"

#: ../xfce4-session/xfsm-manager.c:2382
msgid "Session manager must be in idle state when requesting a restart"
msgstr "Նիստի կառավարիչը պէտք չգործի, վերագործարկման հրամանի հարցման ժամանակ"

#: ../xfce4-session/xfsm-logout-dialog.c:193
#, c-format
msgid "Log out %s"
msgstr "Դուրս գալ %s֊ից"

#. *
#. * Logout
#. *
#: ../xfce4-session/xfsm-logout-dialog.c:238
msgid "_Log Out"
msgstr "_Աւարտել նիստը"

#: ../xfce4-session/xfsm-logout-dialog.c:258
msgid "_Restart"
msgstr "_Վերագործարկել"

#: ../xfce4-session/xfsm-logout-dialog.c:278
msgid "Shut _Down"
msgstr "Ան_ջատել"

#: ../xfce4-session/xfsm-logout-dialog.c:302
msgid "Sus_pend"
msgstr "Նիր_հել"

#: ../xfce4-session/xfsm-logout-dialog.c:336
msgid "_Hibernate"
msgstr "_Քնել"

#: ../xfce4-session/xfsm-logout-dialog.c:367
msgid "H_ybrid Sleep"
msgstr "Հ_ամակցուած քուն"

#: ../xfce4-session/xfsm-logout-dialog.c:398
msgid "Switch _User"
msgstr "Փոխել _Աւգտատիրոջը"

#: ../xfce4-session/xfsm-logout-dialog.c:423
msgid "_Save session for future logins"
msgstr "_Պահել նիստը, յետագայ մուտքերի համար"

#: ../xfce4-session/xfsm-logout-dialog.c:451
msgid "An error occurred"
msgstr "Սխալ"

#: ../xfce4-session/xfsm-shutdown.c:162
msgid "Shutdown is blocked by the kiosk settings"
msgstr "Անջատումը արգելափակուած է կարգաւորումներից"

#: ../xfce4-session/xfsm-shutdown.c:219
#, c-format
msgid "Unknown shutdown method %d"
msgstr "Անջատման %d անյայտ եղանակ"

#: ../xfce4-session-logout/main.c:70
msgid "Log out without displaying the logout dialog"
msgstr "Դուրս գալ, առանց անջատման երկխաւսութեան ցուցադրման"

#: ../xfce4-session-logout/main.c:74
msgid "Halt without displaying the logout dialog"
msgstr "Դադարեցնել, առանց անջատման երկխաւսութեան ցուցադրման "

#: ../xfce4-session-logout/main.c:78
msgid "Reboot without displaying the logout dialog"
msgstr "Վերագործորկել համակարգիչը, առանց անջատման երկխաւսութեան ցուցադրման"

#: ../xfce4-session-logout/main.c:82
msgid "Suspend without displaying the logout dialog"
msgstr "Նիրհել, առանց անջատման երկխաւսութեան ցուցադրման"

#: ../xfce4-session-logout/main.c:86
msgid "Hibernate without displaying the logout dialog"
msgstr "Քուն, առանց անջատման երկխաւսութեան ցուցադրման"

#: ../xfce4-session-logout/main.c:90
msgid "Hybrid Sleep without displaying the logout dialog"
msgstr "Համակցուած քուն, առանց անջատման երկխաւսութեան ցուցադրման"

#: ../xfce4-session-logout/main.c:94
msgid "Switch user without displaying the logout dialog"
msgstr "Փոխել աւգտատիրոջը, առանց անջատման երկխաւսութեան ցուցադրման"

#: ../xfce4-session-logout/main.c:98
msgid "Log out quickly; don't save the session"
msgstr "Դուրս գալ արագաւրէն, առանց նիստի պահման"

#: ../xfce4-session-logout/main.c:121
msgid "Unknown error"
msgstr "Անյայտ սխալ"

#: ../xfce4-session-logout/main.c:147
msgid "Written by Benedikt Meurer <benny@xfce.org>"
msgstr "Հեղինակներ․ Բենեդիկտ Միւրեր <benny@xfce.org>"

#: ../xfce4-session-logout/main.c:148
msgid "and Brian Tarricone <kelnos@xfce.org>."
msgstr "եւ Բրայան Տարրիկոնէ <kelnos@xfce.org>."

#: ../xfce4-session-logout/main.c:168 ../xfce4-session-logout/main.c:279
msgid "Received error while trying to log out"
msgstr "Սխալ՝ անջատման փորձի ժամանակ"

#: ../xfce4-session-logout/main.c:244
#, c-format
msgid "Received error while trying to log out, error was %s"
msgstr "%s -ը անջատման փորձի ժամանակ հայտնուած սխալն է"

#: ../xfce4-session-logout/xfce4-session-logout.desktop.in.h:2
msgid "Log out of the Xfce Desktop"
msgstr "Դուրս գալ Xfce֊ի աշխատասեղանից"

#: ../settings/xfce-session-settings.desktop.in.h:1
#: ../settings/xfce4-session-settings.ui.h:1
msgid "Session and Startup"
msgstr "Նիստ եւ մեկնարկ"

#: ../settings/xfce-session-settings.desktop.in.h:2
msgid "Customize desktop startup"
msgstr "Յարմարեցնել աշխատասեղանի մեկնարկումը"

#: ../settings/xfce-session-settings.desktop.in.h:3
msgid ""
"session;settings;preferences;manager;startup;login;logout;shutdown;lock "
"screen;application;autostart;launch;services;daemon;agent;"
msgstr ""

#: ../settings/xfce4-session-settings.ui.h:2
msgid "_Help"
msgstr "_Աջակցութիւն"

#: ../settings/xfce4-session-settings.ui.h:4
msgid "_Display chooser on login"
msgstr "_Ցուցադրել ընտրիչը, մուտքի ժամանակ"

#: ../settings/xfce4-session-settings.ui.h:5
msgid "Display the session chooser every time Xfce starts"
msgstr "Ցուցադրել նիստի ընտրիչն Xfce֊ի ամեն գործարկման ժամանակ"

#: ../settings/xfce4-session-settings.ui.h:6
msgid "<b>Session Chooser</b>"
msgstr "<b>Նիստի ընտրիչ</b>"

#: ../settings/xfce4-session-settings.ui.h:7
msgid "Automatically save session on logo_ut"
msgstr "Ինքաշխատաւրէն պահել նիստը, դուրս գալ_ուց"

#: ../settings/xfce4-session-settings.ui.h:8
msgid "Always save the session when logging out"
msgstr "Միշտ պահել նիստը դուրս գալուց"

#: ../settings/xfce4-session-settings.ui.h:9
msgid "Pro_mpt on logout"
msgstr "Հու_շել, դուրս գալուց առաջ"

#: ../settings/xfce4-session-settings.ui.h:10
msgid "Prompt for confirmation when logging out"
msgstr "Դուրս գալուց՝ հուշել հաստատման մասին"

#: ../settings/xfce4-session-settings.ui.h:11
msgid "<b>Logout Settings</b>"
msgstr "<b>Դուրս գալու կարգաւորումներ</b>"

#: ../settings/xfce4-session-settings.ui.h:12
msgid "Lock screen be_fore sleep"
msgstr "Կողպել ցուցադրիչը, _մինչ հանգիստը"

#: ../settings/xfce4-session-settings.ui.h:13
msgid "Run xflock4 before suspending or hibernating the system"
msgstr "Գործարկել xflock4֊ը քնից կամ նիրհումից առաջ։"

#: ../settings/xfce4-session-settings.ui.h:14
msgid "<b>Shutdown</b>"
msgstr "<b>Անջատել</b>"

#: ../settings/xfce4-session-settings.ui.h:15
msgid "_General"
msgstr "_Ընդհանուր"

#: ../settings/xfce4-session-settings.ui.h:16
msgid "Currently active session: <b>Default</b>"
msgstr "Ներկայումս գործուն նիստ․ <b>Լռելեայն</b>"

#: ../settings/xfce4-session-settings.ui.h:17
msgid "Save Sess_ion"
msgstr "Պահել նիս_տը"

#: ../settings/xfce4-session-settings.ui.h:18
msgid ""
"These applications are a part of the currently-running session, and can be "
"saved now or when you log out.  Changes below will only take effect when the"
" session is saved."
msgstr ""

#: ../settings/xfce4-session-settings.ui.h:20
msgid "Current Sessio_n"
msgstr "Ներկայումս  նիս_տ"

#: ../settings/xfce4-session-settings.ui.h:21
msgid "Delete the selected session"
msgstr "Ջնջել ընտրուած նիստը"

#: ../settings/xfce4-session-settings.ui.h:22
msgid "Clear Save_d Sessions"
msgstr "Մաքրել պահու_ած նիստերը"

#: ../settings/xfce4-session-settings.ui.h:23
msgid "Saved _Sessions"
msgstr "Պահուած _նիստեր"

#: ../settings/xfce4-session-settings.ui.h:24
msgid "Launch GN_OME services on startup"
msgstr "Գործարկել _GNOME ծառայութիւնները միացման ժամանակ"

#: ../settings/xfce4-session-settings.ui.h:25
msgid "Start GNOME services, such as gnome-keyring"
msgstr "Գործարկել GNOME ծառայութիւնները, աւրինակ՝ gnome-keyring-ը։"

#: ../settings/xfce4-session-settings.ui.h:26
msgid "Launch _KDE services on startup"
msgstr "Գործարկել _KDE ծառայութիւնները միացման ժամանակ"

#: ../settings/xfce4-session-settings.ui.h:27
msgid "Start KDE services, such as kdeinit"
msgstr "Գործարկել KDE ծառայութիւններն, աւրինակ՝ kdeinit֊ը"

#: ../settings/xfce4-session-settings.ui.h:28
msgid "<b>Compatibility</b>"
msgstr "<b>Համատեղելիութիւն</b>"

#: ../settings/xfce4-session-settings.ui.h:29
msgid "Manage _remote applications"
msgstr "Ծրագերի _հեռակայ կառավարում"

#: ../settings/xfce4-session-settings.ui.h:30
msgid ""
"Manage remote applications over the network (this may be a security risk)"
msgstr "Կառավարել հեռակայ ծրագրերը ցանցով (վտանգաւոր է)"

#: ../settings/xfce4-session-settings.ui.h:31
msgid "<b>Security</b>"
msgstr "<b>Պաշտպանութիւն</b>"

#: ../settings/xfce4-session-settings.ui.h:32
msgid "Ad_vanced"
msgstr "_Յառաջացած"

#: ../settings/xfce4-session-settings.ui.h:33
msgid "Saving Session"
msgstr "Պահել նիստը"

#: ../settings/xfce4-session-settings.ui.h:34
msgid ""
"Your session is being saved.  If you do not wish to wait, you may close this"
" window."
msgstr "Ձեր նիստը պահուում է։ Եթե չէք ցանկանում սպասել, կարող էք փակել այս պատուհանը։"
